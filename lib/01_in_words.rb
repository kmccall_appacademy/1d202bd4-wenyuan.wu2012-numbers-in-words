class Fixnum

  def in_words
    if self < 10
      single_digit(self)
    elsif self < 100
      double_digit(self)
    elsif self < 1000
      three_digit(self)
    elsif self < 1000000
      thousand(self)
    elsif self < 1000000000
      millions(self)
    elsif self < 1000000000000
      billions(self)
    elsif self < 1000000000000000
      trillions(self)
    end
  end

  private
  def single_digit(num)
    case num
    when 0
      "zero"
    when 1
      "one"
    when 2
      "two"
    when 3
      "three"
    when 4
      "four"
    when 5
      "five"
    when 6
      "six"
    when 7
      "seven"
    when 8
      "eight"
    when 9
      "nine"
    end
  end

  def ten_to_twelve(num)
    case num
    when 10
      "ten"
    when 11
      "eleven"
    when 12
      "twelve"
    end
  end

  def teens(num)
   case num
   when 13
     "thirteen"
   when 14
     "fourteen"
   when 15
     "fifteen"
   when 16
     "sixteen"
   when 17
     "seventeen"
   when 18
     "eighteen"
   when 19
     "nineteen"
   end
  end

  def tens(num)
    case num / 10
    when 2
      "twenty"
    when 3
      "thirty"
    when 4
      "forty"
    when 5
      "fifty"
    when 6
      "sixty"
    when 7
      "seventy"
    when 8
      "eighty"
    when 9
      "ninety"
    end
  end

  def double_digit(num)
    if num < 13
      ten_to_twelve(num)
    elsif num < 20
      teens(num)
    elsif num % 10 == 0
      tens(num)
    else
      "#{tens(num)} #{single_digit(num % 10)}"
    end
  end

  def three_digit(num)
    # $stderr.puts num%100
    # $stderr.puts "#{single_digit(num / 100)}"
    if num % 100 == 0
      "#{single_digit(num / 100)} hundred"
    else
      "#{single_digit(num / 100)} hundred #{double_digit(num  - (num/100)*100)}"
    end
  end

  def triage(num)
    if num / 10 == 0
      single_digit(num)
    elsif num / 100 == 0
      double_digit(num)
    else
      three_digit(num)
    end
  end

  def thousand(num)
    before_thousand = num / 1000
    after_thousand = num - (num / 1000) * 1000
    if num % 1000 == 0
      "#{triage(before_thousand)} thousand"
    elsif before_thousand == 0
      "#{triage(after_thousand)}"
    else
      "#{triage(before_thousand)} thousand #{triage(after_thousand)}"
    end
  end

  def millions(num)
    before_million = num / 1000000
    after_million = num - (num / 1000000) * 1000000
    if num % 1000000 == 0
      "#{triage(before_million)} million"
    elsif before_million == 0
      "#{thousand(after_million)}"
    else
      "#{triage(before_million)} million #{thousand(after_million)}"
    end
  end

  def billions(num)
    before_billion = num / 1000000000
    after_billion = num - (num / 1000000000) * 1000000000
    if num % 1000000000 == 0
      "#{triage(before_billion)} billion"
    elsif before_billion == 0
      "#{millions(after_billion)}"
    else
      "#{triage(before_billion)} billion #{millions(after_billion)}"
    end
  end

  def trillions(num)
    before_trillion = num / 1000000000000
    after_trillion = num - (num / 1000000000000) * 1000000000000
    if num % 1000000000000 == 0
      "#{triage(before_trillion)} trillion"
    elsif before_trillion == 0
      "#{billions(after_trillion)}"
    else
      "#{triage(before_trillion)} trillion #{billions(after_trillion)}"
    end
  end

end
